﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PongGameManager : MonoBehaviour
{
    bool ballLaunched = false;
    bool ballOffscreen = false;
    bool gameOver = false;

    public Text pongScoreText;
    public Text gameOverText;

    int scoreP1;
    int scoreP2;
    public int winScore = 7;

    float exitTime;

    Vector3 defaultBallPosition;

    public Camera cam;
    BallController ball;
    PaddleController paddle;

    AudioSource sound;
    public AudioSource P1ScoreSound;
    public AudioSource P2ScoreSound;
    bool P1scsored = false;

    void Start()
    {
        cam = FindObjectOfType<Camera>();
        ball = FindObjectOfType<BallController>();
        paddle = FindObjectOfType<PaddleController>();
        ballLaunched = false;
        gameOver = false;
    }

    private void Update()
    {
        PlayUpdate();
    }

    void PlayUpdate()
    {
        //Ball Launching when you Press 'Space'

        if (!ballLaunched)
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                ballLaunched = true;
                if (!P1scsored)
                    ball.SetVelocity(Vector3.right * 18);
                else
                    ball.SetVelocity(Vector3.left * 18);
                P1scsored = false;
            }
            else
            {
                Vector3 currentPaddlePos = paddle.transform.position;
                Vector3 newBallPos = new Vector3(0, 0, 0);
                ball.transform.position = newBallPos;
            }
        }

        //Scoring and Resetting the Ball
        Vector3 ballPos = cam.WorldToViewportPoint(ball.transform.position);
        if (ballPos.x < 0)
        {
            ballOffscreen = true;
            exitTime = Time.time;
            scoreP2++;
            ball.SetVelocity(new Vector3(0, 0, 0));
            ball.transform.position = new Vector3(-2.59f, 0, 0);
            ballLaunched = false;
            PlayUpdate();
        }
        else if (ballPos.x > 1)
        {
            ballOffscreen = true;
            exitTime = Time.time;
            scoreP1++;
            P1scsored = true;
            ball.SetVelocity(new Vector3(0, 0, 0));
            ball.transform.position = new Vector3(-2.59f, 0, 0);
            ballLaunched = false;
            PlayUpdate();
        }
    }
    
    void GameOver()
    {
        //Declaring the Winner
        if (scoreP1 == winScore)
        {
            gameOver = true;
            gameOverText.enabled = true;
            gameOverText.text = "Player 1 Wins";
        }
        else if (scoreP2 == winScore)
        {
            gameOver = true;
            gameOverText.enabled = true;
            gameOverText.text = "Player 2 Wins";
        }
    }
}
