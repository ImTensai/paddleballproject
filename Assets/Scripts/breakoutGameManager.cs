﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BreakoutGameManager : MonoBehaviour
{
    public int rows = 3;
    public int columns = 7;
    public Vector2 spacing = Vector2.one;
    public float yOffset = 2;
    public float dropDuration = 0.5f;
    public float scaleduration = 0.1f;
    public float exitTime;

    public Camera cam;
    public GameObject brickPrefab;
    BallController ball;
    PaddleController paddle;

    public int numBricksLeft;
    public Text gameOverText;
    Canvas gameOverPanel;
    bool gameOver = false;
    bool ballOffscreen = false;
    bool bricksPlaced = false;
    bool ballLaunched = false;

    public Text scoreText;
    public Text highScoreText;
    int score = 0;
    int highScore = 0;

    public Text livesText;
    int lives = 3;
    public int maxLives = 3;

    private void Start()
    {
        cam = FindObjectOfType<Camera>();
        ball = FindObjectOfType<BallController>();
        paddle = FindObjectOfType<PaddleController>();
        score = 0;
        highScore = PlayerPrefs.GetInt("highScore", 0);
        gameOverText.enabled = false;
        livesText.text = "Lives: " + lives;
        livesText.enabled = true;
        scoreText.text = "Score: " + score;
        scoreText.enabled = true;
        highScoreText.text = "High Score: " + highScore;
        highScoreText.enabled = true;
        ball.SetVelocity(Vector3.zero);
        lives = maxLives;
        StartCoroutine(PlaceBricksCoroutine());
    }
        //Placing the Bricks
    IEnumerator PlaceBricksCoroutine()
    {
        numBricksLeft = rows * columns;
        float xOffset = (columns * 0.5f) - .5f;

        for(int i = 0; i < columns; i++)
        {
            for (int j = 0; j < rows; j++)
            {
                GameObject brick = Instantiate(brickPrefab);
                brick.transform.position = new Vector3((i - xOffset) * spacing.x, j * spacing.y + yOffset + 7, 0) ;
                yield return new WaitForSeconds(0.1f);
            }
        }
        bricksPlaced = true;
    }
        // Launching the Ball After the Bricks are Placed
    private void Update()
    {
        if(!ballLaunched)
        {
            if (Input.GetKeyDown(KeyCode.Space) && bricksPlaced)
            {
                ballLaunched = true;
                ball.SetVelocity(Vector3.up * 20);
            }
            else
            {
                Vector3 currentPaddlePos = paddle.transform.position;
                Vector3 newBallPos = new Vector3(currentPaddlePos.x, ball.transform.position.y, ball.transform.position.z);
                ball.transform.position = newBallPos;
            }
        }
        if(gameOver && lives == 0 || gameOver && numBricksLeft == 0)
        {
            if(Input.GetKeyDown(KeyCode.Space))
            {
                ResetBall();
                gameOver = false;
                Start();
            }
        }
        UpdateLivesText();
    }

    private void ResetBall()
    {
        ballOffscreen = false;
        ballLaunched = false;
    }
        //Lives System
    private void UpdateLivesText()
    {
        Vector3 ballPos = cam.WorldToViewportPoint(ball.transform.position);

        if (ballPos.y < 0 && !ballOffscreen)
        {
            exitTime = Time.time;
            lives -= 1;

            if (lives > 0)
            {
                ball.SetVelocity(new Vector3(0, 0, 0));
                ball.transform.localPosition = Vector3.zero;
                ballOffscreen = true;
                ResetBall();
            }

            else
            {
                ball.SetVelocity(new Vector3(0, 0, 0));
                ball.transform.localPosition = Vector3.zero;
                GameOver();
            }
        }
        livesText.text = "Lives: " + lives;
    }
        //Scoring System
    public void ScorePoints (int points)
    {
        numBricksLeft -= 1;
        score += points;
        scoreText.text = "Score: " + score;
        GameOver();
    }

    public void GameOver()
    {       //Outcome of the Game
        if (numBricksLeft == 0)
        {
            gameOver = true;
            gameOverText.enabled = true;
            gameOverText.text = "Oh You Think You're Good " + " 'Press Space to Continue'";
            ball.SetVelocity(new Vector3(0, 0, 0));
            ball.transform.localPosition = Vector3.zero;
        }

        else if (numBricksLeft > 0 && lives == 0)
        {
            gameOver = true;
            gameOverText.enabled = true;
            gameOverText.text = "Hold your Lose " + " 'Press Space to Try Again'";
            ball.SetVelocity(new Vector3(0, 0, 0));
            ball.transform.localPosition = Vector3.zero;
        }
            //Displaying the new Highscore
        if (score > highScore)
        {
            highScoreText.text = scoreText.text;
            highScoreText.text = "High Score: " + score;
            PlayerPrefs.SetInt("highScore", score);
        }
    }
    /*private void GameOver()
    {
        if (lives == 0)
        {
            gameOver = true;
            gameOverPannel.SetActive(true);
            gameOverText.enabled = true;
            gameOverText.text = "You Lose.";
            ball.transform.SetParent(player1.transform);
            ball.transform.localPosition = defaultBallPositiion;
        }
        else
        {
            int numOfBricksBroken = 0;

            for (int b = 0; b < Bricks.Length; b++)
            {
                if (!Bricks[b].gameObject.activeInHierarchy)
                {
                    numOfBricksBroken++;
                }
            }
            if (numOfBricksBroken == Bricks.Length)
            {
                
            }
        }
    }

    
    

    void ResetBall()
    {
        if (isBreakout)
        {
            ball.transform.SetParent(player1.transform);
            ball.transform.localPosition = defaultBallPositiion;
            ball.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezeRotation;
        }
        else
        
        

        
        ballOffscreen = false;

        Vector3 pos = player1.transform.position;
        pos.y = 0;
        player1.transform.position = pos;
        if (!isBreakout)
        {
            ball.transform.position = new Vector3(0, 0, 0);
            pos = player2.transform.position;
            pos.y = 0;
            player2.transform.position = pos;
        }
    }

    private void Update()
    {
        if (!gameOver)
        {//Breakout
            if(lives == 0)
            {
                gameOver = true;
                gameOverPannel.SetActive(true);
                gameOverText.enabled = true;
                gameOverText.text = "You Lose.";
                ball.transform.SetParent(player1.transform);
                ball.transform.localPosition = defaultBallPositiion;
            }
            else 
            {
                int numOfBricksBroken = 0;

                for (int b = 0; b < Bricks.Length; b++)
                {
                    if (!Bricks[b].gameObject.activeInHierarchy)
                    {
                        numOfBricksBroken++;
                    }
                }
                if (numOfBricksBroken == Bricks.Length)
                {
                    gameOver = true;
                    gameOverPannel.SetActive(true);
                    gameOverText.enabled = true;
                    gameOverText.text = "You Win!";
                }
            }
            // pong
            
            else
            {
                if (!ballOffscreen)
                {
                    PlayUpdate();
                }

            }
        }
        else
        {
            if (Input.anyKeyDown)
            {
                ball.transform.localPosition = new Vector3(0, 1.25f, 0);
                ball.SetVelocity(new Vector3(0, 0, 0));
                ResetBricks();
                Awake();
            }
        }
    }

    void ResetBricks()
    {
        
        for(int i = 0; i< Bricks.Length; i++)
        {
            Bricks[i].gameObject.SetActive(true);
        }
    }*/
}
