﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallController : MonoBehaviour
{
    bool isBreakout = true;
    public ParticleSystem particles;
    public Transform mesh;
    public Vector3 maxStretch = new Vector3(1.2f, 0.8f, 0.8f);
    public float minStretchSpeed = 5;
    public float maxStretchSpeed = 20;
    [Range(0, 1)] public float hitInfluence = 0.5f;
    public float speed = 10;
    public Vector3 initialDirection = new Vector3(1, 0, 0);
    [Range(0, 1)] public float minBounceVolume = 1;
    [Range(0, 1)] public float pitchRange = 2f;
    bool isBouncing = false;
    bool Play = false;
    public float bounceDuration = 1;
    public float bounceSpeed = 10;

    public AudioClip bounceSound;
    public AudioClip paddleSound;
    AudioSource sound;

    TrailRenderer trail;
    Rigidbody body;

    
    private void Awake()
    {
        if (isBreakout)
        {
            sound = GetComponent<AudioSource>();
            body = GetComponent<Rigidbody>();
            body.velocity = initialDirection * speed;
        }
    }

    public void Hit(Vector3 dir)
    {
        if (isBreakout)
        {
            dir.Normalize();
            dir = Vector3.Lerp(body.velocity.normalized, dir, hitInfluence);
            dir.Normalize();
            body.velocity = dir * speed;
        }
    }

    public void SetVelocity(Vector3 velocity)
        //Setting the Ball Speed
    {
        body.velocity = velocity;
    }

    private void OnCollisionEnter(Collision c)
    {   //Playing the Bounce Sounds
        if (bounceCoroutine() != null)
        {
            StopCoroutine(bounceCoroutine());
        }

        StartCoroutine(bounceCoroutine());
        if (c.gameObject.GetComponent<PaddleController>() != null)
        {
            sound.clip = paddleSound;
        }
        else
        {
            sound.clip = bounceSound;
        }

        particles.transform.up = body.velocity.normalized;
        particles.transform.position = transform.position;
        particles.Play();
    }
    IEnumerator bounceCoroutine()
    {
        yield return null;
    }

    private void Update()
    {
        if (isBouncing) return;
        {
            float frac = Mathf.Clamp01((speed - minStretchSpeed) / (maxStretchSpeed - minStretchSpeed));
            mesh.localScale = Vector3.Lerp(Vector3.one, maxStretch, frac);
            mesh.right = body.velocity.normalized;
        }
    }

    

    public void SetDirection(Vector3 dir)
    {
        if (isBreakout)
        {
            dir = Vector3.Lerp(body.velocity.normalized, dir, hitInfluence);
            dir.Normalize();
            body.velocity = dir * speed;
        }
    }
}
