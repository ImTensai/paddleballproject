﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisappearingBricks : MonoBehaviour
{
    BreakoutGameManager breakoutGameManager;
    private void Start()
    {
        breakoutGameManager = FindObjectOfType<BreakoutGameManager>();
    }
    private void OnCollisionExit(Collision collision)
        //Making the Bricks Disappear
    {
        gameObject.SetActive(false);
        breakoutGameManager.ScorePoints(100);
    }
}
