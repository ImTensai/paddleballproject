﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PaddleController : MonoBehaviour
{
    public bool auto = false;
    public float speed = 10;
    public string axisName = "Vertical";
    public Vector3 dir = new Vector3(0, 1, 0);
    Rigidbody body;
    BallController ball;

    private void Start()
    {
        body = GetComponent<Rigidbody>();
        ball = FindObjectOfType<BallController>();
    }

    void Update()
    {
        float t = Input.GetAxis(axisName);
        transform.position += dir * t * speed * Time.deltaTime;
            //Bounce Off the Paddle
        if (auto)
        {
            Vector3 toBall = ball.transform.position - transform.position;
            t = Vector3.Dot(toBall, dir);
            t = Mathf.Clamp(t, -1, 1);
        }
        else
        {
            t = Input.GetAxis(axisName);
        }
        body.velocity = dir * t * speed;
    }

    private void OnCollisionExit(Collision c)
    {
        BallController ball = c.gameObject.GetComponent<BallController>();
        if (ball == null)
        {
            return;
        }
    }
}
